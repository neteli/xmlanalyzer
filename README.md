# Smart XML Analyzer
## How it works
We take all attribute names, attribute values and element text and concatenate them into single
string which represents one element in input file. Then we use string comparison algorithm which
calculates distance between two strings (original element and element from target file).
As the closest element to element from original file we take element with the smallest distance value.

## How to run
```
java -jar testtask-1.0.0-jar-with-dependencies.jar <original_file> <target_file>  *<original_element_id>
```
Program arguments:
- original_file - original file
- target_file - file in which we will search for similar element
- original_html_element_id - Optional argument, it's an element id to search in original file, if not set then default value is="make-everything-ok-button" will be used

example:
```
java -jar testtask-1.0.0-jar-with-dependencies.jar src/test/resources/sample-0-origin.html src/test/resources/sample-1-evil-gemini.html
```

## Output samples:
Output for sample-1-evil-gemini.html
```
execute SmartXmlAnalyzer
original file: /home/adv/work/n-agileengine/testtask/src/test/resources/sample-0-origin.html
target file: /home/adv/work/n-agileengine/testtask/src/test/resources/sample-1-evil-gemini.html
original html element id: make-everything-ok-button

closest target element <a class="btn btn-success" href="#check-and-ok" title="Make-Button" rel="done" onclick="javascript:window.okDone(); return false;"> Make everything OK </a>
closest target element path: html > body > div > div > div > div > div > div > a
```

Output for sample-4-the-mash.html
```
execute SmartXmlAnalyzer
original file: /home/adv/work/n-agileengine/testtask/src/test/resources/sample-0-origin.html
target file: /home/adv/work/n-agileengine/testtask/src/test/resources/sample-4-the-mash.html
original html element id: make-everything-ok-button

closest target element <a class="btn btn-success" href="#ok" title="Make-Button" rel="next" onclick="javascript:window.okFinalize(); return false;"> Do all GREAT </a>
closest target element path: html > body > div > div > div > div > div > div > a
```
## System requirements:
Java 1.8+