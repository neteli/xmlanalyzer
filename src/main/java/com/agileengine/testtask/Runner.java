package com.agileengine.testtask;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Optional;

public class Runner {

    public static String PREDEF_ORIGINAL_ELEMENT_ID = "make-everything-ok-button";
    private static Logger LOGGER = LoggerFactory.getLogger(Runner.class);

    private Runner() {

    }

    public static void main(String[] args) {
        LOGGER.info("execute SmartXmlAnalyzer");
        String originPath;
        String targetPath;
        String originalElementId = PREDEF_ORIGINAL_ELEMENT_ID;
        if (args.length == 2) {
            originPath = args[0];
            targetPath = args[1];
        } else if (args.length == 3) {
            originPath = args[0];
            targetPath = args[1];
            originalElementId = args[2];
        } else {
            throw new IllegalArgumentException("Expected 2 or 3 arguments:\n" +
                    "<input_origin_file_path> <input_other_sample_file_path> *<original_element_id>\noriginalElementId is optional argument.");
        }

        LOGGER.info("original file: " + originPath + "\ntarget file: " + targetPath + "\noriginal html element id: " + originalElementId);
        Optional<Element> closestElement = new SearchEngine().findClosest(new File(originPath), new File(targetPath), originalElementId);
        String result = closestElement.map(e -> {
            String path = SearchEngine.getElementPath(e);
            return "\nclosest target element " + e.toString() +
                    "\nclosest target element path: " + path;
        }).orElse("No similar element found in target file=" + targetPath);
        LOGGER.info(result);
    }
}
