package com.agileengine.testtask;

import info.debatty.java.stringsimilarity.NormalizedLevenshtein;
import info.debatty.java.stringsimilarity.interfaces.NormalizedStringDistance;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SearchEngine {

    private static String CHARSET_NAME = "utf8";
    private Logger LOGGER = LoggerFactory.getLogger(SearchEngine.class);
    private NormalizedStringDistance SIMILARITY_CALC = new NormalizedLevenshtein();

    public Optional<Element> findClosest(File originalFile, File targetFile, String originalElementId) {
        Optional<Element> originalElement = findElementById(originalFile, originalElementId);
        Optional<String> originalAttributesOpt = originalElement.map(e -> stringifyAttributes(e));

        if (!originalAttributesOpt.isPresent()) {
            LOGGER.warn("Original file=" + originalFile.getAbsolutePath() + " doesn't contain mandatory element with id=" + originalElementId);
        }

        return originalAttributesOpt.flatMap(originalAttributes -> {
            try {
                Stream<Element> targetElements = collectElements(targetFile);
                return targetElements.map(e -> Pair.of(e, calcSimilarity(originalAttributes, e)))
                        .min(Comparator.comparing(Pair::getValue)).map(Pair::getKey);
            } catch (IOException e) {
                LOGGER.error("Error reading [{}] file" + targetFile.getAbsolutePath(), e);
                return Optional.empty();
            }
        });
    }

    public static String getElementPath(Element closestElement) {
        Iterator<Element> iterator = closestElement.parents().iterator();
        List<String> parentsList = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED),
                false).map(Element::nodeName)
                .collect(Collectors.toList());
        Collections.reverse(parentsList);
        return parentsList.stream().collect(Collectors.joining(" > ")) + " > " + closestElement.nodeName();
    }

    private Double calcSimilarity(String originalAttributes, Element e) {
        String targetAttributes = stringifyAttributes(e);
        Double similarity = SIMILARITY_CALC.distance(originalAttributes, targetAttributes);
        return similarity;
    }

    Stream<Element> collectElements(File htmlFile) throws IOException {
        Document doc = Jsoup.parse(
                htmlFile,
                CHARSET_NAME,
                htmlFile.getAbsolutePath());

        return doc.body().getAllElements().stream();

    }

    private Optional<Element> findElementById(File htmlFile, String targetElementId) {
        try {
            Document doc = Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath());

            return Optional.ofNullable(doc.getElementById(targetElementId));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file" + htmlFile.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    private String stringifyAttributes(Element element) {
        return element.attributes().asList().stream()
                .map(attr -> attr.getKey() + " = " + attr.getValue()).sorted()
                .collect(Collectors.joining(",")) + "," + element.text();
    }
}
