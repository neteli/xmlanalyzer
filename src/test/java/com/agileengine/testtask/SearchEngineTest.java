package com.agileengine.testtask;

import org.jsoup.nodes.Element;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.*;

public class SearchEngineTest {
    private File originalFile = new File("/home/adv/work/n-agileengine/testtask/src/test/resources/sample-0-origin.html");

    @Test
    public void findSimilar_01() throws Exception {
        File targetFile = new File("src/test/resources/sample-1-evil-gemini.html");
        String expected =
                "<a class=\"btn btn-success\" href=\"#check-and-ok\" title=\"Make-Button\" rel=\"done\" onclick=\"javascript:window.okDone(); return false;\"> Make everything OK </a>";
        testFindSimilar(targetFile, expected);
    }

    @Test
    public void findSimilar_02() throws Exception {
        File targetFile = new File("src/test/resources/sample-2-container-and-clone.html");
        String expected =
                "<a class=\"btn test-link-ok\" href=\"#ok\" title=\"Make-Button\" rel=\"next\" onclick=\"javascript:window.okComplete(); return false;\"> Make everything OK </a>";
        testFindSimilar(targetFile, expected);
    }

    @Test
    public void findSimilar_03() throws Exception {
        File targetFile = new File("src/test/resources/sample-3-the-escape.html");
        String expected =
                "<a class=\"btn btn-success\" href=\"#ok\" title=\"Do-Link\" rel=\"next\" onclick=\"javascript:window.okDone(); return false;\"> Do anything perfect </a>";
        testFindSimilar(targetFile, expected);
    }

    @Test
    public void findSimilar_04() throws Exception {
        File targetFile = new File("src/test/resources/sample-4-the-mash.html");
        String expected =
                "<a class=\"btn btn-success\" href=\"#ok\" title=\"Make-Button\" rel=\"next\" onclick=\"javascript:window.okFinalize(); return false;\"> Do all GREAT </a>";
        testFindSimilar(targetFile, expected);
    }

    @Test
    public void notExistingOriginalFile() throws Exception {
        Optional<Element> result = new SearchEngine().findClosest(new File("/not/existing/path/file"), originalFile, Runner.PREDEF_ORIGINAL_ELEMENT_ID);
        assertFalse(result.isPresent());
    }

    @Test(expected = IOException.class)
    public void notExistingTargetFile() throws Exception {
        new SearchEngine().collectElements(new File("/not/existing/path/file"));
    }

    private void testFindSimilar(File targetFile, String expected) {
        Optional<Element> result = new SearchEngine().findClosest(originalFile, targetFile, Runner.PREDEF_ORIGINAL_ELEMENT_ID);
        assertTrue("target element must be found in target document", result.isPresent());
        assertEquals(expected, result.get().toString());
    }

}